package com.example.task5

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [DB1::class],version = 1)
abstract class Database : RoomDatabase() {
    abstract fun getDao(): Dao
}