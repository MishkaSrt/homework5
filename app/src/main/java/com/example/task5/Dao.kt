package com.example.task5
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
@Dao
interface Dao {
    @Query("SELECT * FROM TABLE1")
    fun getAllData() : List<DB1>
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg data: DB1)

}