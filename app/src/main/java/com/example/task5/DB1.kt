package com.example.task5
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "TABLE1")
data class DB1(
    @PrimaryKey(autoGenerate = true)
    val id : Int,
    @ColumnInfo(name = "RUNDISTANCE")
    val RUNDISTANCE : String,
    @ColumnInfo(name = "SWIMDISTANCE")
    val SWIMDISTANCE : String,
    @ColumnInfo(name = "CALORIES")
    val CALORIES : String
)
