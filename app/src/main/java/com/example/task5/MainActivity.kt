package com.example.task5

import android.content.ContentValues
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var distanceRun : EditText
    private lateinit var distanceSwim : EditText
    private lateinit var calories : EditText
    private lateinit var saveBtn : Button
    private lateinit var dataOutput : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        distanceRun = findViewById(R.id.distanceRun)
        distanceSwim = findViewById(R.id.distanceSwim)
        calories = findViewById(R.id.calories)
        saveBtn = findViewById(R.id.saveBtn)
        dataOutput = findViewById(R.id.dataOutput)


        val helper = MyDBHelper(applicationContext)
        val db = helper.readableDatabase

        saveBtn.setOnClickListener{
            val cv = ContentValues()
            cv.put("RUNDISTANCE",distanceRun.text.toString())
            cv.put("SWIMDISTANCE",distanceSwim.text.toString())
            cv.put("CALORIES",calories.text.toString())
            db.insert("TABLE1",null, cv)

            Toast.makeText(applicationContext,distanceRun.text.toString(),Toast.LENGTH_SHORT).show()
            Toast.makeText(applicationContext,distanceSwim.text.toString(),Toast.LENGTH_SHORT).show()
            Toast.makeText(applicationContext,calories.text.toString(),Toast.LENGTH_SHORT).show()

            distanceRun.setText("")
            distanceSwim.setText("")
            calories.setText("")



//            App.instance.db.getDao().insert(DB1(0,"${distanceRun.text.toString().toInt()}","${distanceSwim.text.toString().toInt()}","${calories.text.toString().toInt()}"))

        }

    }

}